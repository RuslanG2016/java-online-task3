package com.epam.Shapes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Shapes2Application {

	public static void main(String[] args) {
		SpringApplication.run(Shapes2Application.class, args);
	}

}
