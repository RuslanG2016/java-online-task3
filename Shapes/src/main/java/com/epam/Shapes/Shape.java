package com.epam.Shapes;

abstract class  Shape {
    abstract void draw();
    abstract void fillColor();

}
