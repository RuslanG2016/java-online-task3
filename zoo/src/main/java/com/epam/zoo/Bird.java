package com.epam.zoo;

public class Bird implements Animal {
    public void sleep(){
        System.out.println("I can sleep");

    }

    @Override
    public void eat() {
        System.out.println("I can eat ");
    }

    @Override
    public void doSomething() {
        System.out.println("I can fly ");
    }
}
