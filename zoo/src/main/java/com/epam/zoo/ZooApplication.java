package com.epam.zoo;


import com.epam.zoo.birds.Eagle;
import com.epam.zoo.birds.Parrot;
import com.epam.zoo.fish.FlyingFish;
import com.epam.zoo.fish.Shark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ZooApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZooApplication.class, args);

		ArrayList <Animal> zoo = new ArrayList<>();
		zoo.add(new Parrot());
		zoo.add(new Eagle());
		zoo.add(new Shark());
		zoo.add(new FlyingFish());

		for (Animal x: zoo){
			System.out.println(x.toString());
			x.sleep();
			x.eat();
			x.doSomething();
			System.out.println("---------------------------------------");
		}



	}

}
