package com.epam.zoo.fish;

import com.epam.zoo.Fish;

public class FlyingFish extends Fish {

    @Override
    public void sleep(){
        System.out.println("I can sleep");

    }

    @Override
    public void eat() {
        System.out.println("I can eat plankton ");
    }

    @Override
    public void doSomething() {
        System.out.println("I can fly over water");
    }
}
