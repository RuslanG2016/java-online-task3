package com.epam.zoo.fish;

import com.epam.zoo.Fish;

 public class Shark extends Fish {
     public void sleep(){
         System.out.println("I can sleep on the bootom of sea");

     }

     @Override
     public void eat() {
         System.out.println("I can eat smaller Fish ");
     }

     @Override
     public void doSomething() {
         System.out.println("I can swim very quickly ");
     }

}
