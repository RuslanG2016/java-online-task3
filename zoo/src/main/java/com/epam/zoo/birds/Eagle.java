package com.epam.zoo.birds;

import com.epam.zoo.Bird;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;


public class Eagle extends Bird {
    public void sleep(){
        System.out.println("I can sleep 5 hours");

    }

    @Override
    public void eat() {
        System.out.println("I can eat meat");
    }

    @Override
    public void doSomething() {
        System.out.println("I can fly very high and fast ");
    }
}
