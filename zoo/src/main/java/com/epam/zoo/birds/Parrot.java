package com.epam.zoo.birds;

import com.epam.zoo.Bird;
import org.springframework.stereotype.Component;

@Component
public class Parrot extends Bird {
    public void sleep(){
        System.out.println("I can sleep hours");

    }

    @Override
    public void eat() {
        System.out.println("I can eat plants and fruits");
    }

    @Override
    public void doSomething() {
        System.out.println("I can talk with human's voice");
    }

}
