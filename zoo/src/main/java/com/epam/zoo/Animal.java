package com.epam.zoo;

interface Animal {
    public void sleep();
    public void eat();
    public void doSomething();

}